import Cards from '../Cards/Cards'

const CardList = (props) => {
    return(
        <>
            {props.myRegister.map(x=>
                <div key={x.id}> 
                    <Cards 
                        userRepo={x.full_name} 
                        description={x.description}
                        urlLink={x.html_url}
                        image={x.owner.avatar_url}
                    />  
                </div>
            )}
        </>
    )
}

export default CardList