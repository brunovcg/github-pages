import {MyInput, MyButton} from './styles'

const SearchEngine = (props) =>{

    return(
        <>
            <MyInput 
                placeholder="Type 'user/repo' then search" 
                value={props.inputed} 
                onChange={x=>props.change(x)}
            />

            <MyButton onClick={()=>props.pressButton()}>Search</MyButton>
        </>
    )
}

export default SearchEngine
