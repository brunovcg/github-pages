import styled from 'styled-components'


const MyInput = styled.input`

    height: 30px;
    width:200px;
    border: none;
    border-top-left-radius: 5px;
    border-bottom-left-radius: 5px;
    padding-left:5px;
`

const MyButton = styled.button`

    width:60px;
    background-color: #0987D7;
    color: white;
    border: none;
    height: 32px;
    border-top-right-radius:5px;
    border-bottom-right-radius:5px;
  
`

export { MyInput, MyButton }






