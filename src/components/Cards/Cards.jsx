import MyCard from './styles'

const Cards = (props) => {
    return(
        <MyCard>
            <div className="imgBox">
                <img src={props.image} alt="img" />
            </div>
            <div className="contentBox">
               <div className="userRepo">{props.userRepo}</div>
               <div className="description">{props.description}</div>
               <a className="urlLink" href={props.urlLink} target="_black">Click to go there!</a>
            </div>
        </MyCard>
    )
}

export default Cards