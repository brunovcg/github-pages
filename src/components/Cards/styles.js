import styled from 'styled-components'


const MyCard = styled.div`

width: 60vw;
background-color: white;
min-height: 80px;
border-radius: 5px;
display: flex;
padding: 10px;
margin-bottom: 10px;

    .imgBox {
        display: flex;
        justify-content: center;
        align-items: center;
        width: 20%;
       
        }
            img {
                border-radius: 50%;
                width: 50px;
                height: 50px;
            }

    .contentBox {
        display: flex;
        flex-direction: column;
        justify-content: center;
        width: 80%;
        padding-right:20px;
    }
        .userRepo {
            text-align: start;
            height: 30%;
            width:100%;
            font-size: 14px;
        }

        .description {
            display:flex;
            align-items: center;
            text-align: start ;
            color: gray;
            font-size: 12px;
            height: 50%;
            width:100%;
        }

        .urlLink {
            text-align: start;
            font-size: 12px;
            height: 20%;
            width:100%;
        }


    `
export default MyCard