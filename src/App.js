import { useState} from 'react'
import SearchEngine from './components/SearchEngine/SearchEngine'
import './App.css';
import CardList from './components/CardList/CardList'

function App() {

  const [myRegister, setMyRegister] = useState([])
  const [inputed, setInputed] = useState("")
  const [emptyError, setEmptyError] = useState("")
  const [apiError, setApiError] = useState("")

  const restartInput = () => {setInputed("")}

  const onChange = (x) => {setInputed(x.target.value); setEmptyError(""); setApiError("")}

  const pressButton = () => {search(inputed)}

  const search = (userReposit) => {
    if (userReposit!==""){
        fetch(`https://api.github.com/repos/${userReposit}`)

        .then(response=> { 
          if(response.status === 200) {response.json() 
            .then((response) => {setMyRegister([...myRegister,response])})
          } else {setApiError("Repo not Found!")}})      

    } else {setEmptyError("ERROR: You have to type 'user/repo' to search")}

    restartInput()
  }

  return (
    <div className="App">
      <div className="App-header">

        <div className="searchBox">
           <SearchEngine 
                restartInput={restartInput} 
                search={search} 
                inputed={inputed}
                pressButton={pressButton}
                change={onChange}
            />
        </div>

        <div className="errorBox">
          {emptyError}{apiError}
        </div>

        <div className="cardListBox"> 
           <CardList myRegister={myRegister}/>
        </div>

      </div>
    </div>
  );
}

export default App;
